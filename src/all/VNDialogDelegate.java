package all;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CustomUIPanelPlugin;
import com.fs.starfarer.api.campaign.CustomVisualDialogDelegate;
import com.fs.starfarer.api.ui.CustomPanelAPI;

public class VNDialogDelegate implements CustomVisualDialogDelegate {
    CustomPanelAPI panel;
    VNDialog vnDialog;
    DialogCallbacks callbacks;
    public VNDialogDelegate(VNDialog vnDialog){
        this.vnDialog=vnDialog;
    }
    @Override
    public void init(CustomPanelAPI panel, DialogCallbacks callbacks) {
        this.panel=panel;
        this.callbacks=callbacks;
        vnDialog.setupPanel(panel);
        vnDialog.setCallbacks(callbacks);
    }

    @Override
    public CustomUIPanelPlugin getCustomPanelPlugin() {
        return vnDialog;
    }

    @Override
    public float getNoiseAlpha() {
        return 0;
    }

    @Override
    public void advance(float amount) {

    }

    @Override
    public void reportDismissed(int option) {

    }
}
