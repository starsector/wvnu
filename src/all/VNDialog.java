package all;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CustomUIPanelPlugin;
import com.fs.starfarer.api.campaign.CustomVisualDialogDelegate;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.*;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.campaign.CoreRuleTokenReplacementGeneratorImpl;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.ui.*;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.*;
import java.util.List;

public class VNDialog implements CustomUIPanelPlugin {
    public boolean cgMode = false;
    CustomVisualDialogDelegate.DialogCallbacks dialogCallbacks;
    public static VNDialog ref;
    float ratioChange;
    FramedCustomPanelPlugin outerBoxp;
    Logger log = Global.getLogger(this.getClass());
    CustomPanelAPI panel;
    CustomPanelAPI text;
    CustomPanelAPI outerBox;
    PositionAPI p;
    SpriteAPI bg;
    SpriteAPI bgf;
    String bgr="";//background reference
    Float bga=1f;
    List<Option> dOptions = new ArrayList<>();
    List<SpriteAPI> c = new ArrayList<>(3);
    List<SpriteAPI> cf = new ArrayList<>(3);
    List<SpriteAPI> t = new ArrayList<>(3);
    List<SpriteAPI> tf = new ArrayList<>(3);
    List<Float> ca =new ArrayList<>(3);
    List<Float> cy = new ArrayList<>(3);
    List<Float> ta =new ArrayList<>(3);
    List<Float> cfa =new ArrayList<>(3);
    List<Float> tfa =new ArrayList<>(3);
    //positions of the titles
    List<Float> tpos =new ArrayList<>(3);
    List<Float> tfpos =new ArrayList<>(3);



    Integer focus=7;
    Boolean bgs =false;

    float offset;
    int activeboxformousemove =-1;
    float bgOffset;//if bg is too wide
    float changeInBgWidth;
    float bgh;
    float bgw;
    TooltipMakerAPI tooltipMakerAPIDialog;
    List<LabelAPI> dialogLabels=new ArrayList<>();
    CustomPanelAPI dialogBox;
    List<FramedCustomPanelPlugin> optionsp=new ArrayList<>();
    List<CustomPanelAPI> options=new ArrayList<>();
    List<TooltipMakerAPI> tooltipMakerAPIOptions=new ArrayList<>();
    List<LabelAPI> optionsLabel=new ArrayList<>();
    List<PositionAPI> labelPositions=new ArrayList<>();
    List<String> additionalText = new ArrayList<>();
    RuleAPI currentRuleAPI;
    String currentRule;
    String trigger;
    CustomPanelAPI optionsContainer =null;
    InteractionDialogAPI interactionDialogAPI;
    Map<String, MemoryAPI> map;
    int scale;
    int lower;
    public VNDialog(String texture,int scale, int lower) {
        this.scale=scale;
        this.lower=lower;
        this.bg = Global.getSettings().getSprite(texture);//"path"
        bgr=texture;
        ratioChange = Global.getSettings().getScreenHeight()/scale;//magic number
        //c, cf,ca,cs,cfs
        c.add(Global.getSettings().getSprite("graphics/wvnu/null.png"));
        c.add(Global.getSettings().getSprite("graphics/wvnu/null.png"));
        c.add(Global.getSettings().getSprite("graphics/wvnu/null.png"));
        t.add(Global.getSettings().getSprite("graphics/wvnu/null.png"));
        t.add(Global.getSettings().getSprite("graphics/wvnu/null.png"));
        t.add(Global.getSettings().getSprite("graphics/wvnu/null.png"));
        cf.add(null);
        cf.add(null);
        cf.add(null);
        tf.add(null);
        tf.add(null);
        tf.add(null);
        ca.add(1f);
        ca.add(1f);
        ca.add(1f);
        ta.add(1f);
        ta.add(1f);
        ta.add(1f);
        cfa.add(0f);
        cfa.add(0f);
        cfa.add(0f);
        tfa.add(0f);
        tfa.add(0f);
        tfa.add(0f);
        tpos.add(0f);
        tpos.add(0f);
        tpos.add(0f);
        tfpos.add(0f);
        tfpos.add(0f);
        tfpos.add(0f);
        cy.add(0f);
        cy.add(0f);
        cy.add(0f);
    }

    @Override
    public void positionChanged(PositionAPI position) {
        p=position;
    }

    @Override
    public void renderBelow(float alphaMult) {
        //1 behind background
        //from example UI love you Alex
        float x = p.getX();
        float y = p.getY();
        float w = p.getWidth();
        float h = p.getHeight();

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        Color color = Color.black;

        GL11.glColor4ub((byte) color.getRed(),
                (byte) color.getGreen(),
                (byte) color.getBlue(),
                (byte) (color.getAlpha() * alphaMult * 0.80f));

        GL11.glBegin(GL11.GL_QUADS);
        {
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x, y + h);
            GL11.glVertex2f(x + w, y + h);
            GL11.glVertex2f(x + w, y);
        }
        GL11.glEnd();
        //2 background
        //swap if applicable
        if(bgf!=null){
            bga=bga-0.01f;
            if(bga<=0){
                bga=0f;
                bgs=false;
                bg=bgf;
                bgf=null;
            }
        } else if(bga<1f) {
            bga = bga + 0.01f;
        }
        if(bga>1f){
            bga=1f;
        }
        //scale background
        if (!bgs) {
            for(int i=0;i<3;i++){
                if(c.get(i)==null){
                    continue;
                }
                //cs.set(i, false);
                // c.set(i,Global.getSettings().getSprite(cStore.get(i)));
                // ce.set(i,Global.getSettings().getSprite(ceStore.get(i)));
                //scale
                // c.get(i).setWidth(c.get(i).getWidth() * ratioChange);
                //c.get(i).setHeight(c.get(i).getHeight() * ratioChange);
                //ce.get(i).setWidth(ce.get(i).getWidth() * ratioChange);
                //ce.get(i).setHeight(ce.get(i).getHeight() * ratioChange);

            }
            bgh = bg.getHeight();
            bgw = bg.getWidth();
            float hwr = bgh / bgw;
            bg.setHeight(p.getHeight());
            float changeInBgHeight = bg.getHeight() - bgh;
            //if(hwr>=1) {
            //    changeInBgWidth = (changeInBgHeight) * hwr;
            //}else{
            //    changeInBgWidth= -1 *(changeInBgHeight) / hwr;
            // }
            bg.setWidth(bg.getHeight()/hwr);

            offset = (w - bg.getWidth()) / 2;
            bgOffset=offset;
            if(offset<0){
                offset=0;
            }
            bgs = true;
        }
        bg.setAlphaMult(bga);
        bg.render(bgOffset, 0);


        if(bg.getWidth()>p.getWidth()){
            if (c.get(0) != null) {
                drawChar((p.getWidth() / 8) + offset, 0, focus % 2 == 1);
                drawLabel((p.getWidth() / 8) + offset, 0, focus % 2 == 1);
            }
            if (c.get(2) != null) {
                //draw in right
                //c3.render((float) , -70);
                //c3.setAlphaMult(c3a);
                drawChar((float) (p.getWidth() * .875) + offset, 2, focus >= 4);
                drawLabel((float) (p.getWidth() * .875) + offset, 2, focus >= 4);
            }
            if (c.get(1) != null) {
                //draw in center
                //c2.render((offset - c2.getWidth() + w) / 2, -70);
                //c2.setAlphaMult(c2a);
                drawChar((p.getWidth() / 2) + offset, 1, focus % 4 > 1);
                drawLabel((p.getWidth() / 2) + offset, 1, focus % 4 > 1);
            }
        }else {
            if (c.get(0) != null) {
                drawChar((bg.getWidth() / 6) + offset, 0, focus % 2 == 1);
                drawLabel((bg.getWidth() / 6) + offset, 0, focus % 2 == 1);
            }
            if (c.get(2) != null) {
                //draw in right
                //c3.render((float) , -70);
                //c3.setAlphaMult(c3a);
                drawChar((float) (bg.getWidth() * .833) + offset, 2, focus >= 4);
                drawLabel((float) (bg.getWidth() * .833) + offset, 2, focus >= 4);
            }
            if (c.get(1) != null) {
                //draw in center
                //c2.render((offset - c2.getWidth() + w) / 2, -70);
                //c2.setAlphaMult(c2a);
                drawChar((bg.getWidth() / 2) + offset, 1, focus % 4 > 1);
                drawLabel((bg.getWidth() / 2) + offset, 1, focus % 4 > 1);
            }
        }

        //different panel

    }
    public void drawChar(float xpos, int x,Boolean focus){
        //if fade in is not null
        //c= character sprite
        //cf= character fade sprite
        float targetAlpha;
        if(focus){
            targetAlpha=1f;
        }else{
            targetAlpha=.5f;
        }
        if(cf.get(x)==null){
            //no fade sprite
            //bring sprite closer to target alpha
                ca.set(x,nudgeAlpha(ca.get(x),targetAlpha));
        }else{
            //there is a fade sprite
            ca.set(x,nudgeAlpha(ca.get(x),0f));
            cfa.set(x,nudgeAlpha(cfa.get(x),targetAlpha));
            //swap sprites
            if(ca.get(x)==0f){
                c.set(x,cf.get(x));
                ca.set(x,cfa.get(x));
                cf.set(x,null);
                cfa.set(x,0f);
            }
        }
        //CY is a boost that characters get when they are focused.
        if(focus){
            cy.set(x,nudgeYpos(cy.get(x),p.getHeight()/50,0.5f));//todo change
        }else{
            cy.set(x,nudgeYpos(cy.get(x),0,0.5f));
        }
        //render
        c.get(x).setAlphaMult(ca.get(x));
        c.get(x).render(xpos-c.get(x).getWidth()/2, -lower+cy.get(x));
        if(cf.get(x)!=null){
            cf.get(x).setAlphaMult(cfa.get(x));
            cf.get(x).render(xpos-cf.get(x).getWidth()/2, -lower+cy.get(x));
        }
    }
    public void drawLabel(float xpos,int x,boolean focus){
        //label starts high, 1/20 screen hight maybe, then goes down to where it sits on fade in, do the same on fade out
        float height = p.getHeight();
        float nudge = height/20;
        float targetAlpha;
        if(focus){
            targetAlpha=1f;
        }else{
            targetAlpha=.5f;
        }
        if(tf.get(x)==null){
            //no fade sprite
            //bring sprite closer to target alpha
            tpos.set(x,nudgeYpos(tpos.get(x),0,0.1f));
            ta.set(x,nudgeAlpha(ta.get(x),targetAlpha));
        }else{
            //there is a fade sprite
            ta.set(x,nudgeAlpha(ta.get(x),0f));//alpha must also be nudged to null //todo
            tpos.set(x,nudgeYpos(tpos.get(x),-nudge,0.1f));
            tfpos.set(x,nudgeYpos(tfpos.get(x),0,0.1f));
            tfa.set(x,nudgeAlpha(tfa.get(x),0f));
            tfa.set(x,nudgeAlpha(tfa.get(x),targetAlpha));
            //swap sprites
            if(ta.get(x)==0f){
                t.set(x,tf.get(x));
                ta.set(x,tfa.get(x));
                tf.set(x,null);
                tfa.set(x,0f);
//                tpos.set(x,tfpos.get(x));//todo new line
            }
        }

        //render
        t.get(x).setAlphaMult(ta.get(x));
        t.get(x).render(xpos-t.get(x).getWidth()/2, -lower + tpos.get(x)+cy.get(x));
        if(tf.get(x)!=null){
            tf.get(x).setAlphaMult(tfa.get(x));
            tf.get(x).render(xpos-tf.get(x).getWidth()/2, -lower + tfpos.get(x)+cy.get(x));
        }

    }
    public float nudgeAlpha(float current, float target){
        if(current<target) {
            current = current + 0.01f;
            if(current>target){
                current=target;
            }
        }else if(current>target){
            current = current - 0.01f;
            if(current<target){
                current=target;
            }
        }
        return current;
    }
    public float nudgeYpos(float current,float target,float speed){
        if(current<target) {
            current = current + speed;
            if(current>target){
                current=target;
            }
        }else if(current>target){
            current = current - speed;
            if(current<target){
                current=target;
            }
        }
        return current;
    }

    @Override
    public void render(float alphaMult) {

    }

    @Override
    public void advance(float amount) {

    }

    @Override
    public void processInput(List<InputEventAPI> events) {
        //panel.bringComponentToTop(text);
        for (InputEventAPI event : events) {
            if (event.isConsumed()) continue;
            if(cgMode){
                if (event.isKeyDownEvent()&&Keyboard.isKeyDown(Keyboard.KEY_1)) {
                    selectOption(0);
                }
                if(event.isMouseDownEvent()){
                    event.consume();
                    selectOption(0);
                }
            }else {
                if (event.isKeyDownEvent()) {
                    if (Keyboard.isKeyDown(Keyboard.KEY_1)) {
                        selectOption(0);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_2)) {
                        selectOption(1);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_3)) {
                        selectOption(2);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_4)) {
                        selectOption(3);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_5)) {
                        selectOption(4);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_6)) {
                        selectOption(5);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_7)) {
                        selectOption(6);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_8)) {
                        selectOption(7);
                    } else if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
                        for (int i = 0; i < dOptions.size(); i++) {
                            if (dOptions.get(i).id.equals("defaultLeave") || dOptions.get(i).id.equals("WvnuBack")) {
                                event.consume();
                                selectOption(i);
                            }
                        }

                    }
                }
//            if(event.isMouseMoveEvent()){
//                for(int i = 0; i<labelPositions.size(); i++){
//                    PositionAPI box = labelPositions.get(i);
//                    float x=Mouse.getX();
//                    float y=Mouse.getY();
//                    if(x>box.getX()&&x<box.getX()+box.getWidth()&&y>box.getY()&&y<box.getY()+ box.getHeight()){
//                        if(activeboxformousemove !=i){
//                            Global.getSoundPlayer().playUISound("ui_button_mouseover",1.0f,1.0f);
//                            activeboxformousemove=i;
//                        }
//                        break;
//                    }
//                }
//            }
                if (event.isMouseDownEvent()) {
                    for (int i = 0; i < labelPositions.size(); i++) {
                        PositionAPI box = labelPositions.get(i);
                        float x = Mouse.getX();
                        float y = Mouse.getY();
                        if (x > box.getX() && x < box.getX() + box.getWidth() && y > box.getY() && y < box.getY() + box.getHeight()) {
                            selectOption(i);
                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void buttonPressed(Object o) {
        //todo button pressed (!) idk what this does
    }

    public void setupPanel(CustomPanelAPI panel){
        this.panel=panel;
        //create panel
        outerBoxp= new FramedCustomPanelPlugin(1,Color.black,false,.4f);
        outerBox= panel.createCustomPanel(1000,300,outerBoxp);
        panel.addComponent(outerBox).inBMid(10f);
    }
    public void initialLoadRule(String currentRule, String trigger, InteractionDialogAPI interactionDialogAPI, Map<String, MemoryAPI> map){
        //store panel in static variable so it doesn't crash saving the whole UI
        //map.get(MemKeys.LOCAL).set("$WvnuRef",this);
        ref = this;

        RuleAPI rule =Global.getSector().getRules().getBestMatching(currentRule,trigger, interactionDialogAPI,map);
        this.map=map;
        this.interactionDialogAPI=interactionDialogAPI;
        loadRule(rule);
    }
    public void loadRule(RuleAPI ruleAPI){
        additionalText= new ArrayList<>();
        optionsp = new ArrayList<>();
        dOptions=new ArrayList<>();
        currentRuleAPI=ruleAPI;
        currentRule=ruleAPI.getId();
        trigger=ruleAPI.getTrigger();
        //make new labels
        dialogLabels = new ArrayList<>();

        for(LabelAPI l:optionsLabel){
            l.setText("");
        }

        dOptions.addAll(ruleAPI.getOptions());

        for(int i = 0;i<ruleAPI.getScriptCopy().size();i++){
            ruleAPI.getScriptCopy().get(i).execute(ruleAPI.getId(),interactionDialogAPI,map);
        }
        //sort lables and put them in the correct order

        List<Option> newOptions = new ArrayList<>();
        //Logger.getLogger(this.getClass()).info(dOptions.size());
        boolean done=false;
        int stopAt = dOptions.size();
        while(!done) {
            int least = Integer.MAX_VALUE;
            Option select = null;
            for (Option o : dOptions) {
                if (o.order < least) {
                    select = o;
                    least= (int) select.order;
                }
            }

            newOptions.add(select);
            dOptions.remove(select);
            if (stopAt == newOptions.size()) {
                //skip 400000 test
                done = true;

            } else if (newOptions.size() == 8) {
                done = true;
                //look for 400000
                for (Option o : dOptions) {
                    if (o.order == 400000) {
                        newOptions.set(7, o);
                    }
                }
            }
        }
            dOptions = newOptions;
        //RENDER DIALOGUE OPTIONS
        FramedCustomPanelPlugin optionsContainerp=new FramedCustomPanelPlugin(1,Color.WHITE,false,0f);
        if(optionsContainer!=null){
            outerBox.removeComponent(optionsContainer);
        }
        optionsContainer = outerBox.createCustomPanel(960,10+dOptions.size()*25,optionsContainerp);
        outerBox.addComponent(optionsContainer).inBMid(5f);
        for (int i = 0; i < dOptions.size(); i++) {
                optionsp.add(i,new FramedCustomPanelPlugin(1,Color.black,false,0));
                options.add(i,optionsContainer.createCustomPanel(950,25,optionsp.get(i)));//CHANGED from 230,50
                tooltipMakerAPIOptions.add(i,options.get(i).createUIElement(950,25,false));//CHANGED from 230,50
                tooltipMakerAPIOptions.get(i).setParaFont(Fonts.INSIGNIA_LARGE);
                if(!cgMode) {
                    optionsLabel.add(i, tooltipMakerAPIOptions.get(i).addPara("" + i, Color.white, 2));
                }
                optionsContainer.addComponent(options.get(i)).inBL(5,5+25*(dOptions.size()-1-i));//CHANGED from (240*x+5,5);
                options.get(i).addUIElement(tooltipMakerAPIOptions.get(i));
                labelPositions.add(i,options.get(i).getPosition());
                if (dOptions.get(i) != null && !cgMode) {
                    optionsLabel.get(i).setText((i + 1) + ":" + dollar(dOptions.get(i).text));
                    optionsLabel.get(i).setAlignment(Alignment.TL);
                }
            }

        //RENDER TEXT BOX
        outerBox.removeComponent(dialogBox);
        FramedCustomPanelPlugin dialogBoxp=new FramedCustomPanelPlugin(1,Color.WHITE,false,0f);
        dialogBox = outerBox.createCustomPanel(960,275-dOptions.size()*25,dialogBoxp);//size of the dialogue box
        outerBox.addComponent(dialogBox).inTMid(5f);
        tooltipMakerAPIDialog=dialogBox.createUIElement(960,275-dOptions.size()*25,true);//size of the dialogue box
        tooltipMakerAPIDialog.setParaFont(Fonts.INSIGNIA_LARGE);
        dialogBox.addUIElement(tooltipMakerAPIDialog);

        List<String> dialogue = new ArrayList<>();
        dialogue = ruleAPI.getText();
//        for(String s : additionalText){
//            log.info(s);
//            dialogue.add(s);
//            log.info("adding from additional text:"+s);
//        }
        if(!cgMode) {
            for (int i = 0; i < dialogue.size(); i++) {
                dialogLabels.add(tooltipMakerAPIDialog.addPara(dollar(dialogue.get(i)), 5));
                log.info("adding 1:" + dialogue.get(i));
                //TODO why was I reusing lables in the past?
                //Log.getLogger().info("making new label");
                //Log.getLogger().info("reusing label");
            }
            for (int i = 0; i < additionalText.size(); i++) {
                dialogLabels.add(tooltipMakerAPIDialog.addPara(dollar(additionalText.get(i)), 5));
            }
        }
        additionalText=new ArrayList<>();




        dialogBox.addUIElement(tooltipMakerAPIDialog);
    }
    public String dollar(String input){
        int addedChars=0;
        if(input.length()==0){
            return input;
        }
        StringBuilder sb = new StringBuilder(input);
        Map<java.lang.String, java.lang.String> tokens = new CoreRuleTokenReplacementGeneratorImpl().getTokenReplacements(this.currentRule, Global.getSector().getCurrentLocation(), this.map);
        for(int i=0;i<sb.length();i++){
            //find dollar
            if(sb.charAt(i)=='$'){
                log.info("found $ at "+i);
                //look at tokens
                StringBuilder selection = new StringBuilder();
                int j=1;
                while(sb.length()>i+j&&(Character.isLetterOrDigit(sb.charAt(i+j))||(sb.charAt(i+j))=='_')){
                    //log.info("checking "+sb.charAt(i+j));
                    j++;
                }
                log.info("selecting from "+i+" to "+(i+j));
                    selection.append(sb.substring(i,i+j));
                    log.info("selecting "+selection);
                    if(tokens.containsKey(selection.toString())){
                        log.info("key found!");
                        sb.replace(i,i+j,tokens.get(selection.toString()));
                        addedChars=tokens.get(selection.toString()).length();
                        //look at local memory
                    }else if(map.get("local").contains(selection.toString())){
                        sb.replace(i,i+j, (String) map.get("local").get(selection.toString()));
                        addedChars=((String)map.get("local").get(selection.toString())).length();
                    }else{
                        //look at memory
                        int k =0;
                        StringBuilder selection2 = new StringBuilder();
                        //while(selection.length()>k&&selection.charAt(k)!='.'){
                        //    k++;
                        //}
                        if(sb.charAt(i+j)=='.'){
                            while(sb.length()>i+j+k+1&&(Character.isLetterOrDigit(sb.charAt(i+j+k+1))||(sb.charAt(i+j+k+1))=='_')){
                                //log.info("checking "+sb.charAt(i+j+k+1));
                                k++;
                            }
                            selection2.append(sb.substring(i+j+1,i+j+k+1));

                            log.info("k="+k);
                            log.info("selection2="+selection2);
                            //log.info("patterns are:"+(selection.substring(1,k))+" AND "+("$"+selection.substring(k+1,selection.length())));
                            if(map.get(selection.substring(1,j)).contains("$"+selection2)){
                                sb.replace(i,i+j+1+k,( map.get(selection.substring(1,j)).get("$"+selection2)).toString());
                                addedChars=(map.get(selection.substring(1,j)).get("$"+selection2)).toString().length();
                            }
                        }
                    }
                //in case something replaced contains $

                i=i+addedChars-1;
            }
        }
        //sb.append(x);
        return sb.toString();
    }
    public void selectOption(int x) {
        if (currentRule==null){
            return;
        }
        if(dOptions.size()<=x)
            return;
        //Global.getSoundPlayer().playUISound("ui_button_pressed",1.0f,1.0f);
        //special case handling
        if(dOptions.get(x).id.equals("defaultLeave")){
            FramedCustomPanelPlugin.cgMode=false;
            if(dialogCallbacks!=null){//case if no master UI
                //Log.getLogger().info("exiting regularly");
                unload();
                ref=null;
                dialogCallbacks.dismissDialog();
            }
        }else if(dOptions.get(x).id.equals("WvnuBack")) {
            FramedCustomPanelPlugin.cgMode=false;
            if (dialogCallbacks != null) {//case if no master UI
                //Log.getLogger().info("exiting regularly");
                unload();
                ref=null;
                dialogCallbacks.dismissDialog();
                return;
            }
        }
        map.get(MemKeys.LOCAL).set("$option",dOptions.get(x).id);
        //tooltipMakerAPIDialog.addPara(dOptions.get(x).text,Color.cyan,5);
        //dialogBox.addUIElement(tooltipMakerAPIDialog);
        loadRule(Global.getSector().getRules().getBestMatching(currentRule,"DialogOptionSelected",interactionDialogAPI,map));
    }
    public void unload()  {
        try {
            JSONObject json = Global.getSettings().getMergedJSONForMod("data/config/WVNULazyLoading.json", "WVNU");
            Iterator<String> i= json.keys();
            String s;
            while(i.hasNext()){
                s=json.getString(i.next());
                Global.getSettings().unloadTexture(s);
            }
        }catch (Exception e){

        }
    }
    public void setCallbacks(CustomVisualDialogDelegate.DialogCallbacks dialogCallbacks){
        this.dialogCallbacks =dialogCallbacks;
    }
    public void setCharacter(int position,String sprite,boolean fade,float scale){
        //c will NOT be null
        //sprite will NOT be null

        //check if that sprite is in that position already
        if(Global.getSettings().getSprite(sprite).equals(cf.get(position))||Global.getSettings().getSprite(sprite).equals(c.get(position))){
            return;
        }
        if(fade) {
            //fade
            //1. complete fade, set c to cf and ca to cfa
            if(cf.get(position)!=null){
                c.set(position,cf.get(position));
                ca.set(position,cfa.get(position));
            }
            //2. set sprite as fade sprite with alpha as 0
            cf.set(position,Global.getSettings().getSprite(sprite));
            cfa.set(position,0f);
            //scale
            cf.get(position).setWidth(cf.get(position).getWidth() * ratioChange*scale);
            cf.get(position).setHeight(cf.get(position).getHeight() * ratioChange*scale);
        }else {
        //not fade
            //1.remove fade sprite
            cf.set(position,null);
            //2.set c sprite
            c.set(position,Global.getSettings().getSprite(sprite));
            //if focused set alpha to 1
            //if not focused set alpha to .5
            if(position==0&&(1 & focus)>0||position==1&&(2 & focus)>0||position==2&&(4 & focus)>0){
                ca.set(position,1f);
            }else{
                ca.set(position,.5f);
            }
            //scale
            c.get(position).setWidth(c.get(position).getWidth() * ratioChange*scale);
            c.get(position).setHeight(c.get(position).getHeight() * ratioChange*scale);
        }
    }

    public void setBg(String bg,Boolean fade){
        if(fade&&!bgr.equals(bg)){
            bgf=Global.getSettings().getSprite(bg);
        }else{
            this.bg=Global.getSettings().getSprite(bg);
            bgs=false;
        }
        bgr=bg;
    }
    public void setTitleCard(int position,String sprite,float scale){
        //c will NOT be null
        //sprite will NOT be null

        float height = p.getHeight();
        float nudge = height/20;


        //check if that sprite is in that position already
        if(Global.getSettings().getSprite(sprite).equals(tf.get(position))||Global.getSettings().getSprite(sprite).equals(t.get(position))){
            return;
        }
            //1. complete fade, set c to cf and ca to cfa
            if(tf.get(position)!=null){
                t.set(position,tf.get(position));
                ta.set(position,tfa.get(position));
            }
            //2. set sprite as fade sprite with alpha as 0
            tf.set(position,Global.getSettings().getSprite(sprite));
            tfa.set(position,0f);
            //scale
            tf.get(position).setWidth(tf.get(position).getWidth() * ratioChange*scale);
            tf.get(position).setHeight(tf.get(position).getHeight() * ratioChange*scale);
            //set positions
            tfpos.set(position,nudge);//todo change
    }
        public void setCgMode(boolean active){
        FramedCustomPanelPlugin.cgMode =active;
        cgMode=active;
    }
    public void setFocus(int focus){
        this.focus=focus;
    }
    public void fireBest(String trigger){
        addFire(Global.getSector().getRules().getBestMatching(currentRule, trigger,interactionDialogAPI,map));
    }
    public void fireAll(String trigger){
        for(RuleAPI r : Global.getSector().getRules().getAllMatching(currentRule,trigger,interactionDialogAPI,map)){
            addFire(r);
        }
    }
    public void addFire(RuleAPI ruleAPI){
        //script
        for(ExpressionAPI e :ruleAPI.getScriptCopy()){
            e.execute(ruleAPI.getId(),interactionDialogAPI,map);
        }
        //text
        int i;
        for(i=0;i<ruleAPI.getText().size();i++){
            additionalText.add(dollar(ruleAPI.getText().get(i)));
                //Log.getLogger().info("making new label");
            //Log.getLogger().info("reusing label");
        }
        dOptions.addAll(ruleAPI.getOptions());
    }
    public static VNDialog getRef(){
    return ref;
    }

}
