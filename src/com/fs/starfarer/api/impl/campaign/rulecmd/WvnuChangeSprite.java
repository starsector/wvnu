package com.fs.starfarer.api.impl.campaign.rulecmd;

import all.VNDialog;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WvnuChangeSprite implements CommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        VNDialog v = VNDialog.getRef();
        String character;
        try {
            character = Global.getSettings().getSpriteName("WVNU", params.get(1).string);
        }catch (RuntimeException e){
            try {
                JSONObject json= Global.getSettings().getMergedJSONForMod("data/config/WVNULazyLoading.json","WVNU");
                Global.getSettings().loadTexture(json.getString(params.get(1).string));
                character=json.getString(params.get(1).string);
            } catch (IOException | JSONException ex) {
                Global.getLogger(this.getClass()).info("SPRITE NOT FOUND:"+params.get(1).string);
                throw new RuntimeException(ex);
            }
        }

        //set scale
        float scale=1f;
        try {
            JSONObject scalingJson = Global.getSettings().getMergedJSONForMod("data/config/WVNUScaling.json","WVNU");
            if(scalingJson.has(params.get(1).string)){
                scale= (float) scalingJson.getDouble(params.get(1).string);
            }
        } catch (Exception e) {
            Global.getLogger(this.getClass()).info("JSON scaling file issue! Falling back to default size!"+params.get(1).string);
        }
        v.setCharacter(Integer.parseInt(params.get(0).string),
                character,
                Boolean.parseBoolean(params.get(2).string),
                scale);
        //Log.getLogger().info(Boolean.getBoolean(params.get(1).string));
        return true;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
}

