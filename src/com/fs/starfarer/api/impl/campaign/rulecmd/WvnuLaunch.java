package com.fs.starfarer.api.impl.campaign.rulecmd;


import all.VNDialog;
import all.VNDialogDelegate;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WvnuLaunch implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        float h = Global.getSettings().getScreenHeight();
        float w =Global.getSettings().getScreenWidth();
        String bg;
        try {
            bg = Global.getSettings().getSpriteName("WVNU", params.get(0).string);
        }catch(RuntimeException e){
            try {
                JSONObject json= Global.getSettings().getMergedJSONForMod("data/config/WVNULazyLoading.json","WVNU");
                Global.getSettings().loadTexture(json.getString(params.get(0).string));
                bg=json.getString(params.get(0).string);
            } catch (IOException | JSONException ex) {
                Global.getLogger(this.getClass()).info("SPRITE NOT FOUND:"+params.get(0).string);
                throw new RuntimeException(ex);
            }
        }
        VNDialog vnd=new VNDialog(bg,Integer.parseInt(params.get(1).string),Integer.parseInt(params.get(2).string));
        memoryMap.get(MemKeys.LOCAL).set("$option",params.get(3).string);
        //vnd.setCharacter1("graphics/thQuest/illustrations/sakuya_th_18_spritesheet_temp.png");
        //memoryMap.get(MemKeys.LOCAL).set("$WvnuRef",vnd);
        dialog.showCustomVisualDialog(w,h,new VNDialogDelegate(vnd));
        vnd.initialLoadRule(s,"WVNU",dialog,memoryMap);
        return true;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
