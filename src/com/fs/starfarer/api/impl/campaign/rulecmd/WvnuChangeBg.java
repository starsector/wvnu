package com.fs.starfarer.api.impl.campaign.rulecmd;

import all.VNDialog;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WvnuChangeBg implements CommandPlugin {
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        VNDialog v= VNDialog.getRef();
        String bg;
        try {
            bg = Global.getSettings().getSpriteName("WVNU", params.get(0).string);
        }catch(RuntimeException e){
            try {
                JSONObject json= Global.getSettings().getMergedJSONForMod("data/config/WVNULazyLoading.json","WVNU");
            Global.getSettings().loadTexture(json.getString(params.get(0).string));
            bg=json.getString(params.get(0).string);
            } catch (IOException | JSONException ex) {
                Global.getLogger(this.getClass()).info("SPRITE NOT FOUND:"+params.get(0).string);
                throw new RuntimeException(ex);
            }
        }
        v.setBg(bg,
                Boolean.parseBoolean(params.get(1).string));
        //Log.getLogger().info(Boolean.getBoolean(params.get(1).string));
        return true;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
}
