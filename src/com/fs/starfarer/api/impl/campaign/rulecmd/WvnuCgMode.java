package com.fs.starfarer.api.impl.campaign.rulecmd;

import all.VNDialog;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WvnuCgMode implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> params, Map<String, MemoryAPI> map) {
        //CGMode hides the UI and automatically selcects the first value available.
        VNDialog v= VNDialog.getRef();
        v.setCgMode(Boolean.parseBoolean(params.get(0).string));
        return true;

    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
