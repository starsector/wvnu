# WVNU

##Changelog

updated to 0.96
Changed view of options.
The size of dialogue and option boxes are based on the number of options
The limit of options has been increased to eight

##How to use
Look in rules.csv for a demo. It is viewable in dev mode.

First put your sprites in WVNU object in settings.json for eager loading. Or in WVNULazyLoading.json for lazy loading. You should put your sprites in WVNULazyLoading to save on vram, but there is a trade-off with load times.

There is a sprite named null that you can use to fade characters and backgrounds out without a replacement sprite.

##WvnuLaunch
Use WvnuLaunch command in a rules.csv script to launch visual novel dialog. It will look in rules.csv for a rule to load with a trigger of WVNU with $option that you set.
The arguments are: 

background scale offset option

background is name of sprite in settings.json or WVNULazyLoading.json.

scale is how big sprites will appear. For example if it is 1000, a 500 px tall sprite will take up half of the screen.

Offset is how many pixels below the viewport the sprites render

option is what $option is set to.

##WvnuChangeSprite
the WvmuChangeSprite command changes a sprite on screen. The arguments are:

position sprite fade

position is 0,1,2. These are from left to right.

sprite is the sprite name.

fade is true or false. If true, it will fade between sprites, if false, there will be no transition.

##WvnuChangeBg
The WvmuChangeSprite command changes the background. Backgrounds are fitted to the screen using their aspect ratio. Sprites are positioned based on background aspect ratio, so I recommend you keep it consistent. The arguments are:

sprite fade

sprite is the sprite name.

fade is true or false. If true, it will fade between backgrounds, if true, there will be no transition.

##WvnuFocus
The WvnuFocus command changes transparency of the sprites to focus the players attention. The argument is:

focus

Focus is a number from 0-7. It is interpreted like binary where the characters are bits. ex. 6 is the second and third character. Positions 1 and 2.
##WvnuFireBest
Identical to FireBest
argument:

trigger

##WvnuFireAll
Identical to FireAll
argument:

trigger

##WvnuChangeMusic
argument:

music id

##Other Things
defaultLeave will bring you back to the campaign layer.
WvnuBack will bring you back to regular dialogue.
a sort order of 400000 will ensure the option is always avalible(used for back options.) It will always appear in the eith slot if there are too many options.

Feel free to use anything in this mod.
###Known issues

